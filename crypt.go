package crypto

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"io"
)

const version byte = 0x84

// Encrypt .
func Encrypt(data, key []byte) ([]byte, error) {
	if len(data) == 0 {
		return nil, nil
	}

	signKey, cryptKey, err := getKey(key)
	if err != nil {
		return nil, err
	}

	n := 1 + aes.BlockSize + len(data)/aes.BlockSize*aes.BlockSize + aes.BlockSize + sha256.Size

	token := make([]byte, n)

	token[0] = version

	// Create random iv
	_, err = io.ReadFull(rand.Reader, token[1:1+aes.BlockSize])
	if err != nil {
		return nil, err
	}

	// copy input data
	copy(token[1+aes.BlockSize:], data)

	// pad data
	c := byte(n - 1 - aes.BlockSize - sha256.Size - len(data))

	copy(token[1+aes.BlockSize+len(data):], bytes.Repeat([]byte{c}, int(c)))

	data = token[1+aes.BlockSize : n-sha256.Size]

	// encrypt data
	cb, _ := aes.NewCipher(cryptKey)
	cipher.NewCBCEncrypter(cb, token[1:1+aes.BlockSize]).CryptBlocks(data, data)

	// generate hmac
	h := hmac.New(sha256.New, signKey)
	h.Write(token[:n-sha256.Size])
	copy(token[n-sha256.Size:], h.Sum(nil))

	return token, nil
}

// Decrypt .
func Decrypt(token, key []byte) ([]byte, error) {
	// Check token len
	if len(token) < 1+aes.BlockSize+sha256.Size ||
		(len(token)-1-aes.BlockSize-sha256.Size)%aes.BlockSize != 0 {
		return nil, ErrInvalidToken
	}

	signKey, cryptKey, err := getKey(key)
	if err != nil {
		return nil, err
	}

	// Check version
	if token[0] != version {
		return nil, ErrInvalidToken
	}

	// Check hmac
	h := hmac.New(sha256.New, signKey)
	h.Write(token[:len(token)-sha256.Size])

	if !bytes.Equal(h.Sum(nil), token[len(token)-sha256.Size:]) {
		return nil, ErrInvalidKey
	}

	iv := token[1 : 1+aes.BlockSize]
	pay := token[1+aes.BlockSize : len(token)-sha256.Size]

	// decrypt data
	cb, _ := aes.NewCipher(cryptKey)
	cipher.NewCBCDecrypter(cb, iv).CryptBlocks(pay, pay)

	// check padding
	c := byte(pay[len(pay)-1])

	for i := len(pay) - int(c); i < len(pay); i++ {
		if pay[i] != c {
			fmt.Println(i, pay[i], c)
			return nil, ErrInvalidToken
		}
	}

	return pay[:len(pay)-int(c)], nil
}
