package crypto

import "testing"

func TestEncryptDecrypt(t *testing.T) {
	const (
		pass = "secret"
		msg  = "abc 123"
	)

	err := SetKey(pass)
	if err != nil {
		t.Fatal(err)
	}

	_, _, err = getKey(nil)
	if err != nil {
		t.Fatal(err)
	}

	token, err := Encrypt([]byte(msg), nil)
	if err != nil {
		t.Fatal(err)
	}

	data, err := Decrypt(token, nil)
	if err != nil {
		t.Fatal(err)
	}

	if string(data) != msg {
		t.Fail()
	}

	_, err = Decrypt(token, []byte("12345678901234567890123456789012"))
	if err != ErrInvalidKey {
		t.Fail()
	}
}
