package crypto

import "errors"

var (
	ErrInvalidPassphrase = errors.New("Invalid passphrase")
	ErrInvalidKey        = errors.New("Invalid key")
	ErrInvalidToken      = errors.New("Invalid token")
)
