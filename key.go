package crypto

import (
	"crypto/sha256"
	"sync"

	"golang.org/x/crypto/pbkdf2"
)

const (
	// KeySize .
	KeySize    = sha256.Size
	iterations = 5000
)

var salt = []byte{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}

var global struct {
	sync.RWMutex
	key []byte
}

// SetKey .
func SetKey(pass string) error {
	if len(pass) < 3 {
		return ErrInvalidPassphrase
	}

	global.Lock()
	global.key = pbkdf2.Key([]byte(pass), salt, iterations, KeySize, sha256.New)
	global.Unlock()

	return nil
}

// ClearKey .
func ClearKey() {
	global.Lock()
	global.key = nil
	global.Unlock()
}

func getKey(key []byte) (signKey, cryptKey []byte, err error) {
	if key == nil {
		global.RLock()
		key = global.key[:]
		global.RUnlock()
	}

	if len(key) != KeySize {
		err = ErrInvalidKey
		return
	}

	signKey = key[:KeySize/2]
	cryptKey = key[KeySize/2:]

	return
}
