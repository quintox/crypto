package crypto

import "database/sql/driver"

// Secret .
type Secret []byte

// Value .
func (s Secret) Value() (driver.Value, error) {
	return Encrypt(s, nil)
}

// Scan .
func (s *Secret) Scan(src interface{}) error {
	if src == nil {
		*s = Secret(nil)
	} else if v, ok := src.([]byte); ok {
		var err error
		*s, err = Decrypt(v, nil)
		if err != nil {
			return err
		}
	}

	return nil
}

// String .
func (s Secret) String() string {
	return string(s)
}
